﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace QRPOC
{
	[Activity (Label = "QRPOC", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it
			Button button = FindViewById<Button> (Resource.Id.myButton);
			EditText amount = FindViewById<EditText> (Resource.Id.editText1);

			button.Click += async (sender, e) =>  {
				var scanner = new ZXing.Mobile.MobileBarcodeScanner(this);
				var result = await scanner.Scan();
				float invoiceAmount = 0;
				if (result != null && parseable(result.ToString(), out invoiceAmount)){
					amount.Text = invoiceAmount.ToString();
				}else{
					Toast toast = new Toast(this);
					toast.Duration = ToastLength.Long;
					toast.SetText("QR no reconocido como e-ticket");
					toast.Show();
				}
			};
		}

		private bool parseable(string url, out float amount){
			amount = 0;
			try {
				var parameters =  url.Split ('?');
				var hasParameters = parameters.Length > 1;
				if (hasParameters) {
					var eticketvalues = parameters [1].Split (',');
					if (eticketvalues.Length >= 4) {
						amount = float.Parse(eticketvalues[4]);
						return true;
					}
				}
			} catch (Exception ex) {
				Console.Write (ex);
				return false;
			}

			return false;
			

		}
	}
}


