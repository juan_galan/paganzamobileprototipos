﻿using System;
using SQLite;

namespace PaganzaMobile.Commons.Database
{
	public abstract class IEntity
	{
		[PrimaryKey, AutoIncrement, Column("_id")]
		public int Id { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime UpdateAt {get;set;}
	}
}

