﻿using System;
using SQLite;
using PaganzaMobile.Commons.Utilities;

namespace PaganzaMobile.Commons.Database
{
	public class DatabaseManager
	{
		public T Insert<T>(T obj, SQLiteConnection db) where T : IEntity
		{
			try
			{
				obj.CreatedAt = new DateTime();
				obj.UpdateAt = new DateTime();
				int id = db.Insert(obj);
				obj.Id = id;

				return obj;
			}
			catch(Exception ex) 
			{
				throw new PaganzaMobileException ("Error while inserting obj ", ex);
			}
		}

		public T Update<T>(T obj, SQLiteConnection db) where T : IEntity
		{
			try
			{
                obj.UpdateAt = new DateTime();
				int id = db.Update(obj);

				return obj;
			}
			catch(Exception ex) 
			{
				throw new PaganzaMobileException ("Error while updating obj ", ex);
			}
		}

        public void Delete<T>(T obj, SQLiteConnection db) where T : IEntity
        {
            try
            {
                obj.UpdateAt = new DateTime();
                int id = db.Delete(obj);
            }
            catch (Exception ex)
            {
                throw new PaganzaMobileException("Error while deleting obj ", ex);
            }
        }
	}
}

