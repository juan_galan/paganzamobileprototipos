﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaganzaMobile.Commons.Utilities
{
    public class ParamsValidation
    {
        public bool IsNameValid(string Name)
        {
            return !String.IsNullOrEmpty(Name) && Name.Length > 1 && Name.Length < 50;
        }

        public bool IsValidYear(int value)
        {
            return value > 1950 && value <= 2070;
        }

        public bool IsValidDay(int value)
        {
            return value > 0 && value <= 31;
        }


        public bool IsValidMonth(int value)
        {
            return value >= 0 && value <= 12;
        }

        public bool IsCurrencyValid(string currency)
        {
            return currency == "UYU" || currency == "USD";
        }

        public void Dispose()
        {

        }

        public bool IsValidDateTime(DateTime date)
        {
            return IsValidDay(date.Day) && IsValidMonth(date.Month) && IsValidYear(date.Year);
        }

        public bool AreDatesValid(DateTime? from, DateTime? to)
        {
            return from.HasValue && to.HasValue && IsValidDateTime(from.Value) && IsValidDateTime(to.Value);
        }

        public bool IsIntValidAsId(int id)
        {
            return id > 0;
        }

        public bool IsAmountValid(long amount)
        {
            return amount > 0 && amount < 1000000;
        }
    }
}
