﻿using System;

namespace PaganzaMobile.Commons.Utilities
{
	public class PaganzaMobileException : Exception
	{
		public PaganzaMobileException()
			: base() { }

		public PaganzaMobileException(string message)
			: base(message) { }

		public PaganzaMobileException(string format, params object[] args)
			: base(string.Format(format, args)) { }

		public PaganzaMobileException(string message, Exception innerException)
			: base(message, innerException) { }

		public PaganzaMobileException(string format, Exception innerException, params object[] args)
			: base(string.Format(format, args), innerException) { }
		
	}
}

