﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaganzaMobile.IControllers;
using PaganzaMobile.Models;

namespace PaganzaMobile.Controllers
{
    public class Reports : IReports
    {
        public MonthResponse GetMonthlyReport(int userId, int year, int month, int? yearTo, int? monthTo, int? serviceId, string currency)
        {
            PaganzaMobile.Access.Reports.ReportsApiAccess access = new PaganzaMobile.Access.Reports.ReportsApiAccess();
            return access.GetMonthReport(userId, year, month, yearTo, monthTo, serviceId, currency);
        }

        public List<ExpenseReport> GetExpensesReport(int userId, int year, int month, int? yearTo, int? monthTo, int? serviceId, string currency)
        {
            throw new NotImplementedException();
        }
    }
}
