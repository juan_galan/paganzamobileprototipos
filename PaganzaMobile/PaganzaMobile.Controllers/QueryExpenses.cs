﻿using System;
using PaganzaMobile.IControllers;
using System.Collections.Generic;
using PaganzaMobile.Models;
using PaganzaMobile.Access.QueryExpenses;
using PaganzaMobile.Commons.Utilities;

namespace PaganzaMobile.Controllers
{
	public class QueryExpenses : IQueryExpenses
	{
		public List<Expense> GetExpensesForUser (int ownerId, int page, DateTime dateFrom, DateTime? dateTo, int? serviceId)
		{
            QueryExpensesApiAccess access = new QueryExpensesApiAccess();
            try
            {
                User owner = this.GetUser();
                List<Expense> query = access.QueryExpensesForUser(owner, page, dateFrom, dateTo, serviceId);

                return query;
            }
            catch (Exception ex)
            {
                throw new PaganzaMobileException("Error while creating expense", ex);
            }
		}
		public Expense GetExpenseForUser (int ownerId, int expenseId)
		{
			throw new NotImplementedException ();
		}

        private User GetUser()
        {
            Users access = Users.Instance;
            return access.GetCurrentUser();
        }

        private Service GetServiceById(int serviceId)
        {
            Services access = Services.Instance;
            return access.GetExpenseServiceById(serviceId);
        }
	}
}

