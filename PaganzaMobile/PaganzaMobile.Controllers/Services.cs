﻿using System;
using PaganzaMobile.IControllers;
using System.Collections.Generic;
using PaganzaMobile.Models;
using PaganzaMobile.Access.Services;

namespace PaganzaMobile.Controllers
{
	public class Services : IServices
	{
		private static Services instance;
		private static bool WereServicesLoaded;

		private Services()
		{
			WereServicesLoaded = false;
			if (!WereServicesLoadedFromServer())
			{
				SaveServerServices();
			}
			else
			{
				WereServicesLoaded = true;
			}
		}

		public static Services Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new Services();
				}
				return instance;
			}
		}

		public List<Service> GetExpenseServicesByPrefix(string prefix)
		{
			ServiceAccess access = new ServiceAccess();
			return access.GetServiceByNamePrefix(prefix);
		}

		public List<Service> GetAllExpenseServices()
		{
			ServiceAccess access = new ServiceAccess();
			List<Service> dbServices = access.GetServices();
			return dbServices;
		}

		public Service GetExpenseServiceById(int serviceId)
		{
			ServiceAccess access = new ServiceAccess();
			return access.GetServiceById(serviceId);
		}

		private bool WereServicesLoadedFromServer()
		{
			ServiceAccess access = new ServiceAccess();
			List<Service> dbServices = access.GetServices();
			return dbServices.Count > 0;
		}

		private void SaveServerServices()
		{
			ServiceAccess access = new ServiceAccess();
			ServicesApiAccess apiAccess = new ServicesApiAccess();
			List<Service> serverServices = apiAccess.GetServices();
			serverServices = access.SaveServerServices(serverServices);
			WereServicesLoaded = true;
		}



		public Service GetPossibleServiceFromName(string name)
        {
            ServiceAccess access = new ServiceAccess();
			return access.GetPossibleServiceFromExpenseName(name);
        }
			
    }
}

