﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaganzaMobile.Access.Companies;
using PaganzaMobile.IControllers;
using PaganzaMobile.Models;

namespace PaganzaMobile.Controllers
{
    public class Companies : ICompany
    {
        public Company GetServiceByNamePrefix(string prefix)
        {
            CompanyAccess access = new CompanyAccess();
            return access.GetServiceByNamePrefix(prefix);
        }

        public Company CreateCompany(string RUT, string Name)
        {
            CompanyAccess access = new CompanyAccess();
            return access.CreateCompany(RUT, Name);
        }

        public string GetCompanyNameFromRUT(string RUT, string defaultName)
        {
            Company possibleCompany = this.GetServiceByNamePrefix(RUT);
            if (possibleCompany != null)
            {
                return possibleCompany.Name;
            }

            return defaultName;
        }
    }
}
