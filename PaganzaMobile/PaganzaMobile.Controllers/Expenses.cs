﻿using System;
using PaganzaMobile.IControllers;
using PaganzaMobile.Models;
using PaganzaMobile.Access;
using PaganzaMobile.Commons.Utilities;

namespace PaganzaMobile.Controllers
{
	public class Expenses : IExpenses
	{
		public Expense CreateExpenseWithRut(int ownerId, string Name, DateTime IssueDate, long TotalAmount, string Currency, string Notes, int serviceId, string RUT)
		{
			ExpensesAccess access = new ExpensesAccess();
			try
			{
				User owner = this.GetUser();
				Service service = this.GetServiceById(serviceId);
                Name = this.GetCompanyNameFromRUT(RUT, Name);

				Expense exp = access.CreateExpense(owner, Name, IssueDate, TotalAmount, Currency, Notes, service);
				if(this.CanExpenseBeUploaded())
				{
					ExpensesApiAccess apiaccess = new ExpensesApiAccess();
					exp = apiaccess.CreateExpenseFromExistingExpense(exp);
				}

				return exp;
			}
			catch(Exception ex)
			{
				throw new PaganzaMobileException("Error while creating expense", ex);
			}
		}

        public Expense CreateExpense(int ownerId, string Name, DateTime IssueDate, long TotalAmount, string Currency, string Notes, int serviceId)
        {
            ExpensesAccess access = new ExpensesAccess();
            try
            {
                User owner = this.GetUser();
                Service service = this.GetServiceById(serviceId);

                Expense exp = access.CreateExpense(owner, Name, IssueDate, TotalAmount, Currency, Notes, service);
                if (this.CanExpenseBeUploaded())
                {
                    ExpensesApiAccess apiaccess = new ExpensesApiAccess();
                    exp = apiaccess.CreateExpenseFromExistingExpense(exp);
                }

                return exp;
            }
            catch (Exception ex)
            {
                throw new PaganzaMobileException("Error while creating expense", ex);
            }
        }

		public Expense ModifyExpense(int expenseId, int ownerId, string Name, DateTime IssueDate, long TotalAmount, string Currency, string Notes, int serviceId, string RUT)
		{
            ExpensesAccess access = new ExpensesAccess();
            try
            {
                User owner = this.GetUser();
                Service service = this.GetServiceById(serviceId);
                Name = this.GetCompanyNameFromRUT(RUT, Name);

                Expense exp = access.GetExpenseById(expenseId);
                exp.Update(TotalAmount, Name, IssueDate, Notes, Currency, "EXP", owner.Id, service.Id, false);

                if (this.CanExpenseBeUploaded())
                {
                    ExpensesApiAccess apiaccess = new ExpensesApiAccess();
                    exp = apiaccess.UpdateExpense(ownerId, Name, IssueDate, TotalAmount, Currency, Notes, serviceId, expenseId);
                }

                return exp;
            }
            catch (Exception ex)
            {
                throw new PaganzaMobileException("Error while creating expense", ex);
            }
		}

		public bool DeleteExpense(int expenseId, int ownerId)
		{
			throw new NotImplementedException();
		}

		private bool CanExpenseBeUploaded()
		{
			return true;
		}

		private User GetUser()
		{
			Users access = Users.Instance;
			return access.GetCurrentUser();
		}

		private Service GetServiceById(int serviceId)
		{
			Services access = Services.Instance;
			return access.GetExpenseServiceById(serviceId);
		}

        private string GetCompanyNameFromRUT(string RUT, string defaultName)
        {
            Companies access = new Companies();
            return access.GetCompanyNameFromRUT(RUT, defaultName);
        }

        public Expense CreateExpenseFromExisting(Expense exp)
        {
            ExpensesAccess access = new ExpensesAccess();
            try
            {
                User owner = this.GetUser();
                Service service = this.GetServiceById(exp.ServiceId);

                exp = access.CreateExpense(owner, exp.Name, exp.IssueDate, exp.Total, exp.Currency, exp.Notes, service);
                if (this.CanExpenseBeUploaded())
                {
                    ExpensesApiAccess apiaccess = new ExpensesApiAccess();
                    exp = apiaccess.CreateExpenseFromExistingExpense(exp);
                }

                return exp;
            }
            catch (Exception ex)
            {
                throw new PaganzaMobileException("Error while creating expense", ex);
            }
        }

        public Expense GetPossibleExpense(string RUT, long TotalAmount, string Currency)
        {
            User owner = this.GetUser();
            DateTime issueDate = DateTime.Now;
            string Name = this.GetCompanyNameFromRUT(RUT, "");
            Service possibleService = this.GetPossibleService(Name);

            return new Expense(TotalAmount, Name, issueDate, "", Currency, "EXP", owner, possibleService, false);
        }

        private Service GetPossibleService(string Name)
        {
            Services access = Services.Instance;
            return access.GetPossibleServiceFromName(Name);

        }
    }
}

