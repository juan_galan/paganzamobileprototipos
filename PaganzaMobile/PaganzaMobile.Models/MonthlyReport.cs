﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaganzaMobile.Models
{
    public class MonthlyReport
    {
        public int Amount { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string Service { get; set; }
        public int ServiceId { get; set; }
        public string Currency { get; set; }
        public string Logo { get; set; }

        public MonthlyReport(int amount, int year, int month, string service, int serviceId, string currency, string logo)
        {
            Amount = amount;
            Year = year;
            Month = month;
            Service = service;
            ServiceId = serviceId;
            Currency = currency;
            Logo = logo;
        }

        public MonthlyReport() { }

    }
}
