﻿using System;
using System.Collections.Generic;
using PaganzaMobile.Commons.Database;

namespace PaganzaMobile.Models
{
	public class User : IEntity
	{
		public User (int userId, string username, string lastname, string name, string email)
		{
            BackendId = userId;
			Username = username;
			Lastname = lastname;
			Name = name;
			Email = email;
			Id = userId;
		}

		public string Username { get; set; }
		public int BackendId {get;set;}
		public string Lastname { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
	}
}

