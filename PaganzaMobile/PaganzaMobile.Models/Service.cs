﻿using System;
using PaganzaMobile.Commons.Database;

namespace PaganzaMobile.Models
{
	public class Service : IEntity
	{
        public Service() { }
		public Service (string name, string logo)
		{
			Name = name;
			Logo = logo;
		}

		public string Name { get; set; }
		public string Logo { get; set; }
        public int BackendId { get; set; }
	}
}

