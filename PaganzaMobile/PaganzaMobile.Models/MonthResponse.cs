﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaganzaMobile.Models
{
    public class MonthResponse
    {
        public Dictionary<string,string> services { get; set; }
        public List<List<MonthlyReport>> data { get; set; }
        public Dictionary<string, string> logos { get; set; }

        public MonthResponse() { }
    }
}
