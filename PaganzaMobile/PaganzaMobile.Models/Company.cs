﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaganzaMobile.Commons.Database;

namespace PaganzaMobile.Models
{
    public class Company : IEntity
	{
        public Company() { }
		public Company (string rUT, string name)
		{
			Name = name;
            RUT = rUT;
		}

		public string RUT { get; set; }
		public string Name { get; set; }
        //public Service LastService { get; set; }
        public int LastServiceId { get; set; }
	}
}
