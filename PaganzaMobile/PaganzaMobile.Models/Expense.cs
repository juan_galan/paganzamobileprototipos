﻿using System;
using PaganzaMobile.Commons.Database;

namespace PaganzaMobile.Models
{
	public class Expense : IEntity
	{
        public Expense()
        { }
        //Internal constructor
		public Expense(long total, string name, DateTime issueDate, string notes, string currency, string type, User u, Service s, bool isOnline)
		{
			//Service = s;
			Total = total;
			IssueDate = issueDate;
			Currency = currency;
			Type = type;
			//Owner = u;
			IsOnline = isOnline;
            Name = name;
            Notes = notes;
            OwnerId = u.Id;
            ServiceId = s.Id;
		}

        //API constructor
        public Expense(long total, string name, DateTime issueDate, string notes, string currency, string type, int ownerId, int serviceId, bool isOnline)
        {
            Total = total;
            IssueDate = issueDate;
            Currency = currency;
            Type = type;
            IsOnline = isOnline;
            Name = name;
            Notes = notes;
            OwnerId = ownerId;
            ServiceId = serviceId;
        }

        public void Update(long total, string name, DateTime issueDate, string notes, string currency, string type, int ownerId, int serviceId, bool isOnline)
        {
            Total = total;
            IssueDate = issueDate;
            Currency = currency;
            Type = type;
            IsOnline = isOnline;
            Name = name;
            Notes = notes;
            OwnerId = ownerId;
            ServiceId = serviceId;
        }


		public int BackendId { get; set; }
		public long Total { get; set; }
		public string Name { get; set; }
        public string Notes { get; set; }
		public DateTime IssueDate { get; set; }
		public string Currency { get; set; }
		public string Type { get; set; }
		public bool IsOnline { get; set; }
		//public User Owner { get; set; }
		//public Service Service { get; set; }
        public int OwnerId { get; set; }
        public int ServiceId { get; set; }
	}
}

