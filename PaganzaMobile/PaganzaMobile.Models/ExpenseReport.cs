﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaganzaMobile.Models
{
    public class ExpenseReport
    {
        public int Amount { get; set; }
        public string Service { get; set; }
        public int ServiceId { get; set; }
        public string Currency { get; set; }
        public string Logo { get; set; }

        public ExpenseReport(int amount, string service, int serviceId, string currency, string logo)
        {
            Amount = amount;
            Service = service;
            ServiceId = serviceId;
            Currency = currency;
            Logo = logo;
        }
    }
}
