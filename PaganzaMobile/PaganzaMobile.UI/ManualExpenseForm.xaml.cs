﻿using System;
using System.Collections.Generic;
using PaganzaMobile.Controllers;
using PaganzaMobile.IControllers;
using PaganzaMobile.Models;

using Xamarin.Forms;

namespace PaganzaMobile.UI
{
	public partial class ManualExpenseForm : ContentPage
	{
		public ManualExpenseForm ()
		{
			InitializeComponent ();
		}

		void SubmitExpense(object sender, EventArgs args)
		{
			IExpenses expenses = new Expenses ();
			var amount = float.Parse(AmountEntry.Text);
			Expense e = expenses.CreateExpense (48, this.NameEntry.Text, this.IssueDateEntry.Date, (long)amount, "UYU", "Notes", 1);
			this.DisplayAlert("Success", "Gasto ingresado!", "OK");
		}
	}
}

