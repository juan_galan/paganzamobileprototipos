﻿using System;

using Acr.BarCodes;
using PaganzaMobile.Controllers;
using PaganzaMobile.IControllers;
using PaganzaMobile.Models;
using Xamarin.Forms; 

namespace PaganzaMobile.UI
{
	public class App : Application
	{
		public App ()
		{
			// The root page of your application
			Button qrScan = new Button();
			qrScan.Text = "Ecanear QR!";

			Button manualExpense = new Button ();
			manualExpense.Text = "Ingreso manual";


			Label l = new Label {
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Start,
				Text = "Paganza Mobile"
			};




			var navigationPage = new NavigationPage(new ContentPage {
				Content = new StackLayout {
					VerticalOptions = LayoutOptions.Center,
					Children = {
						l, qrScan, manualExpense
					}
				}
			});

			qrScan.Clicked += async (sender, args) => {
				var result = await BarCodes.Instance.Read();
				if (!result.Success)
					await MainPage.DisplayAlert("Error", "No se reconoció el código", "OK");
				else {
					float invoiceAmount = 0;
					if (result != null && parseable(result.Code, out invoiceAmount)){
						IExpenses expenses = new Expenses ();
						Expense e = expenses.CreateExpense (48, "Mobile expense", DateTime.Now, (long)invoiceAmount, "UYU", "Notes", 1);
						await MainPage.DisplayAlert("Success", "Gasto ingresado!", "OK");
					}else{
						await MainPage.DisplayAlert("Error", "No se reconoció el código", "OK");
					}
				}
			};

			manualExpense.Clicked += async (object sender, EventArgs e) => {
				await navigationPage.PushAsync(new ManualExpenseForm() );
			};

			MainPage = navigationPage;
		}

		private bool parseable(string url, out float amount){
			amount = 0;
			try {
				var parameters =  url.Split ('?');
				var hasParameters = parameters.Length > 1;
				if (hasParameters) {
					var eticketvalues = parameters [1].Split (',');
					if (eticketvalues.Length >= 4) {
						amount = float.Parse(eticketvalues[4]);
						return true;
					}
				}
			} catch (Exception ex) {
				return false;
			}

			return false;


		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

		void CreateExpense(object sender, EventArgs e)
		{
			//IExpenses expenses = new Expenses ();
			//expenses.CreateExpense (48, "Mobile expense", DateTime.Now, 1000, "UYU", "Notes", 1, "210000000");
		}
	}
}

