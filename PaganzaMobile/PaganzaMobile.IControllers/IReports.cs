﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaganzaMobile.Models;

namespace PaganzaMobile.IControllers
{
    public interface IReports
    {
        MonthResponse GetMonthlyReport(int userId, int year, int month, int? yearTo, int? monthTo, int? serviceId, string currency);
        List<ExpenseReport> GetExpensesReport(int userId, int year, int month, int? yearTo, int? monthTo, int? serviceId, string currency);
    }
}
