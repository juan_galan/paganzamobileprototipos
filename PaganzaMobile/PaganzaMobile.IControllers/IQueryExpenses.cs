﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaganzaMobile.Models;

namespace PaganzaMobile.IControllers
{
    public interface IQueryExpenses
    {
        List<Expense> GetExpensesForUser(int ownerId, int page, DateTime dateFrom, DateTime? dateTo, int? serviceId);
        Expense GetExpenseForUser(int ownerId, int expenseId);
    }
}
