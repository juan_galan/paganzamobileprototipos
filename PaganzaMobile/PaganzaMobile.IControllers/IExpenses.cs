﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PaganzaMobile.Models;

namespace PaganzaMobile.IControllers
{
    public interface IExpenses
    {
        Expense CreateExpense(int ownerId, string Name, DateTime IssueDate, long TotalAmount, string Currency, string Notes, int serviceId);
        Expense CreateExpenseFromExisting(Expense exp);
        Expense GetPossibleExpense(string RUT, long TotalAmount, string Currency);
        Expense CreateExpenseWithRut(int ownerId, string Name, DateTime IssueDate, long TotalAmount, string Currency, string Notes, int serviceId, string RUT);
		Expense ModifyExpense(int expenseId, int ownerId, string Name, DateTime IssueDate, long TotalAmount, string Currency, string Notes, int serviceId, string RUT);
        
        bool DeleteExpense(int expenseId, int ownerId);
    }
}
