﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaganzaMobile.Models;

namespace PaganzaMobile.IControllers
{
    public interface IServices
    {
        List<Service> GetExpenseServicesByPrefix(string prefix);
        List<Service> GetAllExpenseServices();
        Service GetExpenseServiceById(int serviceId);
        Service GetPossibleServiceFromName(string Name);
    }
}
