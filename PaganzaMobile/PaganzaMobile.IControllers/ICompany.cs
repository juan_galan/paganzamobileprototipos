﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaganzaMobile.Models;

namespace PaganzaMobile.IControllers
{
    public interface ICompany
    {
        Company GetServiceByNamePrefix(string prefix);
        Company CreateCompany(string RUT, string Name);
        string GetCompanyNameFromRUT(string RUT, string defaultName);
    }
}
