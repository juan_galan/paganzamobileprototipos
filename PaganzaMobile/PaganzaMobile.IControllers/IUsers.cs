﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaganzaMobile.Models;

namespace PaganzaMobile.IControllers
{
    public interface IUsers
    {
        bool Login(string username, string password);

        User GetCurrentUser();
    }
}
