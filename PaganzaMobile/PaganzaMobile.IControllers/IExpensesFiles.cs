﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Paganza.Core.VNext.Business.Entities;

namespace PaganzaMobile.IControllers
{
    public interface IExpensesFiles
    {
        Task<ExpensesFile> CreateExpensesFile(string FileName, int ExternalExpenseId);
        Task<ExpensesFile> ModifyExpensesFile(string FileName, int ExternalExpenseId, int FileId);
        Task<bool> DeleteExpensesFile(int FileId);
    }
}
