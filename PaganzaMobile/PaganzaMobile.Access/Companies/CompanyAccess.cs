﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaganzaMobile.Models;

namespace PaganzaMobile.Access.Companies
{
    public class CompanyAccess
    {
        public PaganzaMobile.Models.Company GetServiceByNamePrefix(string RUT)
        {
            using (var paganza = new PaganzaMobileDatabaseManager())
            {
                var possibleCompanies = from s in paganza.db.Table<PaganzaMobile.Models.Company>()
                              where s.Name == RUT
                              select s;

                return possibleCompanies.FirstOrDefault();
              
            }
        }

        public PaganzaMobile.Models.Company CreateCompany(string RUT, string Name)
        {

            var newCompany = new PaganzaMobile.Models.Company(RUT, Name);
            using (var paganza = new PaganzaMobileDatabaseManager())
            {
                newCompany = paganza.Insert<PaganzaMobile.Models.Company>(newCompany, paganza.db);
            }

            return newCompany;
        }
    }
}
