﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace PaganzaMobile.Access.QueryExpenses
{
    public class QueryExpensesApiAccess : ApiAccess
    {
        private const string OwnerIdParameter = "userId";
        private const string ExpenseIdParameter = "expenseId";
        private const string PageParameter = "page";
        private const string DayParameter = "day";
        private const string MonthParameter = "month";
        private const string YearParameter = "year";
        private const string DayToParameter = "dayTo";
        private const string MonthToParameter = "monthTo";
        private const string YearToParameter = "yearTo";
        private const string ServiceIdParameter = "serviceId";

        public List<Models.Expense> QueryExpensesForUser(Models.User owner, int page, DateTime dateFrom, DateTime? dateTo, int? serviceId)
        {
            List<Models.Expense> parsedExpenses = new List<Models.Expense>();
            try
            {
                string result = Get("Expenses", this.GetQueryParameters(owner, page, dateFrom, dateTo, serviceId, null));

                JArray a = JArray.Parse(result);
                parsedExpenses = a.ToObject<List<Models.Expense>>();
            }
            catch (Exception ex)
            {

            }

            return parsedExpenses;
        }

        private Dictionary<string, string> GetQueryParameters(Models.User owner, int page, DateTime dateFrom, DateTime? dateTo, int? serviceId, int? expenseId)
        {
            var parameters = new Dictionary<string, string>();
            parameters[OwnerIdParameter] = owner.Id.ToString();
            if (expenseId.HasValue)
            {
                parameters[ExpenseIdParameter] = expenseId.Value.ToString();
            }
            else
            {
                parameters[PageParameter] = page.ToString();
                parameters[DayParameter] = dateFrom.Day.ToString();
                parameters[MonthParameter] = dateFrom.Month.ToString();
                parameters[YearParameter] = dateFrom.Year.ToString();

                if (serviceId.HasValue)
                {
                    parameters[ServiceIdParameter] = serviceId.Value.ToString();
                }

                if(dateTo.HasValue)
                {
                    parameters[DayToParameter] = dateFrom.Day.ToString();
                    parameters[MonthToParameter] = dateFrom.Month.ToString();
                    parameters[YearToParameter] = dateFrom.Year.ToString();
                }
                
            }
            

            return parameters;

        }
    }
}
