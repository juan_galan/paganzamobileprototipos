﻿using System;
using PaganzaMobile.Access.Expenses;
using PaganzaMobile.Commons.Utilities;
using PaganzaMobile.Models;

namespace PaganzaMobile.Access
{
	public class ExpensesAccess
	{
		public Expense CreateExpense(User owner, string Name, DateTime IssueDate, long TotalAmount, string Currency, string Notes, Service service)
        {
            AreParametersValidForExpense(owner.Id, Name, IssueDate, TotalAmount, Currency, service.Id);
            var newExpense = new Expense(TotalAmount, Name, IssueDate, Notes, Currency, "EXP", owner, service, false);
            using (var paganza = new PaganzaMobileDatabaseManager())
            {
                newExpense = paganza.Insert<Expense>(newExpense, paganza.db);
            }

            return newExpense;
        }

        public Expense UpdateExpense(Expense exp)
        {
            AreParametersValidForExpense(exp.OwnerId, exp.Name, exp.IssueDate, exp.Total, exp.Currency, exp.ServiceId);
            using (var paganza = new PaganzaMobileDatabaseManager())
            {
                exp = paganza.Update<Expense>(exp, paganza.db);
            }

            return exp;
        }

        public void DeleteExpense(Expense exp)
        {
            using (var paganza = new PaganzaMobileDatabaseManager())
            {
                exp = paganza.Update<Expense>(exp, paganza.db);
            }
        }

        public Expense GetExpenseById(int expId)
        {
            using (var paganza = new PaganzaMobileDatabaseManager())
            {
                return paganza.db.Get<Expense>(expId);
            }
        }

        private void AreParametersValidForExpense(int ownerId, string Name, DateTime IssueDate, long TotalAmount, string Currency, int ServiceId)
        {
            var paramsValidation = new ParamsValidation();
            
            if (!paramsValidation.IsNameValid(Name))
            {
                throw ExpensesExceptions.ThrowExpenseException(ExpensesExceptions.ExpensesExceptionType.InvalidName);
            }

            if (!paramsValidation.IsValidMonth(IssueDate.Month))
            {
                throw ExpensesExceptions.ThrowExpenseException(ExpensesExceptions.ExpensesExceptionType.InvalidMonth);
            }

            if (!paramsValidation.IsValidYear(IssueDate.Year))
            {
                throw ExpensesExceptions.ThrowExpenseException(ExpensesExceptions.ExpensesExceptionType.InvalidYear);
            }

            if (!paramsValidation.IsValidDay(IssueDate.Day))
            {
                throw ExpensesExceptions.ThrowExpenseException(ExpensesExceptions.ExpensesExceptionType.InvalidDay);
            }

            if (!paramsValidation.IsCurrencyValid(Currency))
            {
                throw ExpensesExceptions.ThrowExpenseException(ExpensesExceptions.ExpensesExceptionType.InvalidCurrency);
            }

            if (!paramsValidation.IsAmountValid(TotalAmount))
            {
                throw ExpensesExceptions.ThrowExpenseException(ExpensesExceptions.ExpensesExceptionType.InvalidAmount);
            }

            if (!paramsValidation.IsIntValidAsId(ServiceId))
            {
                throw ExpensesExceptions.ThrowExpenseException(ExpensesExceptions.ExpensesExceptionType.InvalidAmount);
            }

            if (!paramsValidation.IsIntValidAsId(ownerId))
            {
                throw ExpensesExceptions.ThrowExpenseException(ExpensesExceptions.ExpensesExceptionType.InvalidAmount);
            }
            
        }
	}
}

