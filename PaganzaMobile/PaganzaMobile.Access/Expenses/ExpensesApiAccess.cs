﻿using System;
using PaganzaMobile.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using RestSharp;
using PaganzaMobile.Commons.Utilities;

namespace PaganzaMobile.Access
{
	public class ExpensesApiAccess : ApiAccess
	{
        private const string OwnerIdParameter = "userId";
        private const string ExpenseIdParameter = "expenseId";
        private const string NameParameter = "name";
        private const string DayParameter = "day";
        private const string MonthParameter = "month";
        private const string YearParameter = "year";
        private const string TotalAmountParameter = "totalAmount";
        private const string CurrencyParameter = "currency";
        private const string NotesParameter = "notes";
        private const string ServiceIdParameter = "serviceId";

        public Expense CreateExpense(int ownerId, string Name, DateTime IssueDate, long TotalAmount, string Currency, string Notes, int serviceId)
        {
            var parameters = GetCreateExpensesParameters(ownerId, Name, IssueDate, TotalAmount, Currency, Notes, serviceId);
            try
            {
                var response = Post("Expenses", parameters);
                dynamic expenseJson = JObject.Parse(response);
                var newExpense = new Expense(TotalAmount, Name, IssueDate, Notes, Currency, "EXP", ownerId, serviceId, true);
                newExpense.BackendId = expenseJson.Id;

                return newExpense;

            }
            catch(Exception ex)
            {
                throw new PaganzaMobileException("Could not reach server", ex);
            }

        }

        public Expense UpdateExpense(int ownerId, string Name, DateTime IssueDate, long TotalAmount, string Currency, string Notes, int serviceId, int expenseId)
        {
            var parameters = GetUpdateExpensesParameters(ownerId, Name, IssueDate, TotalAmount, Currency, Notes, serviceId, expenseId);
            try
            {
                var response = Put("Expenses", parameters);
                dynamic expenseJson = JObject.Parse(response);
                var updatedExpense = new Expense(TotalAmount, Name, IssueDate, Notes, Currency, "EXP", ownerId, serviceId, true);
                updatedExpense.BackendId = expenseJson.Id;

                return updatedExpense;

            }
            catch (Exception ex)
            {
                throw new PaganzaMobileException("Could not reach server", ex);
            }

        }

        public Expense UpdateExpenseFromExistingExpense(Expense exp)
        {
            var parameters = GetUpdateExpensesParameters(exp.OwnerId, exp.Name, exp.IssueDate, exp.Total, exp.Currency, exp.Notes, exp.ServiceId, exp.BackendId);
            try
            {
                var response = Put("Expenses", parameters);
                dynamic expenseJson = JObject.Parse(response);
                exp.IsOnline = true;
                exp.BackendId = expenseJson.Id;

                return exp;

            }
            catch (Exception ex)
            {
                throw new PaganzaMobileException("Could not reach server", ex);
            }
        }

        public Expense CreateExpenseFromExistingExpense(Expense exp)
        {
            var parameters = GetCreateExpensesParameters(exp.OwnerId, exp.Name, exp.IssueDate, exp.Total, exp.Currency, exp.Notes, exp.ServiceId);
            try
            {
                var response = Post("Expenses", parameters);
                dynamic expenseJson = JObject.Parse(response);
				//exp.BackendId = expenseJson.Id;

                return exp;

            }
            catch (Exception ex)
            {
                throw new PaganzaMobileException("Could not reach server", ex);
            }
        }

        private Dictionary<string,string> GetCreateExpensesParameters(int ownerId, string Name, DateTime IssueDate, long TotalAmount, string Currency, string Notes, int serviceId)
        {
            var parameters = new Dictionary<string, string>();
            parameters[OwnerIdParameter] = ownerId.ToString();
            parameters[NameParameter] = Name;
            parameters[DayParameter] = IssueDate.Day.ToString();
            parameters[MonthParameter] = IssueDate.Month.ToString();
            parameters[YearParameter] = IssueDate.Year.ToString();
            parameters[TotalAmountParameter] = TotalAmount.ToString();
            parameters[CurrencyParameter] = Currency;
            parameters[NotesParameter] = Notes;
            parameters[ServiceIdParameter] = serviceId.ToString();

            return parameters;
        }

        private Dictionary<string, string> GetUpdateExpensesParameters(int ownerId, string Name, DateTime IssueDate, long TotalAmount, string Currency, string Notes, int serviceId, int expenseId)
        {
            var parameters = new Dictionary<string, string>();
            parameters[OwnerIdParameter] = ownerId.ToString();
            parameters[NameParameter] = Name;
            parameters[DayParameter] = IssueDate.Day.ToString();
            parameters[MonthParameter] = IssueDate.Month.ToString();
            parameters[YearParameter] = IssueDate.Year.ToString();
            parameters[TotalAmountParameter] = TotalAmount.ToString();
            parameters[CurrencyParameter] = Currency;
            parameters[NotesParameter] = Notes;
            parameters[ServiceIdParameter] = serviceId.ToString();
            parameters[ExpenseIdParameter] = expenseId.ToString();

            return parameters;

        }
	}
}

