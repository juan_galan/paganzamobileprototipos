﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaganzaMobile.Commons.Utilities;

namespace PaganzaMobile.Access.Expenses
{
    public class ExpensesExceptions
    {
        public enum ExpensesExceptionType { InvalidName, InvalidMonth, InvalidYear, InvalidDay, InvalidAmount, InvalidCurrency, InvalidServiceId, InvalidOwnerId };
        public static Dictionary<ExpensesExceptionType, string> ExceptionMessages = new Dictionary<ExpensesExceptionType, string>(){
            {ExpensesExceptionType.InvalidAmount, "El monto ingresado debe ser mayor a 0 y menor a 1.000.000"},
            {ExpensesExceptionType.InvalidMonth, "Mes Invalido"},
            {ExpensesExceptionType.InvalidYear, "Año Invalido"},
            {ExpensesExceptionType.InvalidName, "Nombre Invalido"},
            {ExpensesExceptionType.InvalidDay, "Dia Invalido"},
            {ExpensesExceptionType.InvalidServiceId, "Servicio Invalido"},
            {ExpensesExceptionType.InvalidOwnerId, "Usuario Invalido"},
            {ExpensesExceptionType.InvalidCurrency, "Moneda Invalida"}
        };

        public static PaganzaMobileException ThrowExpenseException(ExpensesExceptionType type)
        {
            string message = ExceptionMessages[type];
            return new PaganzaMobileException(message);
        }
    }
}
