﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PaganzaMobile.Models;
using RestSharp;
using RestSharp.Deserializers;

namespace PaganzaMobile.Access.Services
{
    public class ServicesApiAccess : ApiAccess
    {
        public List<Service> GetServices()
        {
            List<Service> services = new List<Service>();
            try
            {
                string result = Get("Services", null);

                JArray a = JArray.Parse(result);
                var parsedServices = a.ToObject<List<Service>>();
                foreach(var service in parsedServices)
                {
                    service.BackendId = service.Id;
                    service.Id = 0;
                    services.Add(service);
                }

            }
            catch (Exception ex)
            {
                
            }

            return services;
        }
    }
}
