﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaganzaMobile.IControllers;
using PaganzaMobile.Models;

namespace PaganzaMobile.Access.Services
{
    public class ServiceAccess
    {
        public List<Service> GetServices()
        {
            using(var paganza = new PaganzaMobileDatabaseManager())
            {
                var services = paganza.db.Table<Service>();
                return services.ToList();
            }
        }

        public List<Service> SaveServerServices(List<Service> serverServices)
        {
            List<Service> dbServices = new List<Service>();
            using(var paganza = new PaganzaMobileDatabaseManager())
            {
                foreach(Service service in serverServices)
                {
                    var dbService = paganza.Insert<Service>(service, paganza.db);
                    dbServices.Add(dbService);
                }
            }

            return dbServices;
        }

        public Service GetServiceById(int serviceId)
        {
            using (var paganza = new PaganzaMobileDatabaseManager())
            {
                return paganza.db.Get<Service>(serviceId);
            }
        }

        public List<Service> GetServiceByNamePrefix(string prefix)
        {
            using (var paganza = new PaganzaMobileDatabaseManager())
            {
                var services = from s in paganza.db.Table<Service>()
                               where s.Name.StartsWith(prefix)
                               select s;

                return services.ToList();
            }
        }

        public Service GetPossibleServiceFromExpenseName(string name)
        {
            using (var paganza = new PaganzaMobileDatabaseManager())
            {
                var possibleExpense = from e in paganza.db.Table<Expense>()
                               where e.Name == name
                               select e;

                var possibleService = possibleExpense != null ? possibleExpense.FirstOrDefault().ServiceId : 0;

                return this.GetServiceById(possibleService);
            }
        }
    }
}
