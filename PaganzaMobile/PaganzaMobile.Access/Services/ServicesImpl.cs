﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaganzaMobile.IControllers;
using PaganzaMobile.Models;

namespace PaganzaMobile.Access.Services
{
    public class ServicesImpl : IServices
    {
        private static ServicesImpl instance;
        private static bool WereServicesLoaded;

        private ServicesImpl()
        {
            WereServicesLoaded = false;
            if (!WereServicesLoadedFromServer())
            {
                SaveServerServices();
            }
            else
            {
                WereServicesLoaded = true;
            }
        }

        public static ServicesImpl Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ServicesImpl();
                }
                return instance;
            }
        }

        public List<Service> GetExpenseServicesByPrefix(string prefix)
        {
            ServiceAccess access = new ServiceAccess();
            return access.GetServiceByNamePrefix(prefix);
        }

        public List<Service> GetAllExpenseServices()
        {
            ServiceAccess access = new ServiceAccess();
            List<Service> dbServices = access.GetServices();
            return dbServices;
        }

        public Service GetExpenseServiceById(int serviceId)
        {
            ServiceAccess access = new ServiceAccess();
            return access.GetServiceById(serviceId);
        }

        private bool WereServicesLoadedFromServer()
        {
            ServiceAccess access = new ServiceAccess();
            List<Service> dbServices = access.GetServices();
            return dbServices.Count > 0;
        }

        private void SaveServerServices()
        {
            ServiceAccess access = new ServiceAccess();
            ServicesApiAccess apiAccess = new ServicesApiAccess();
            List<Service> serverServices = apiAccess.GetServices();
            serverServices = access.SaveServerServices(serverServices);
            WereServicesLoaded = true;
        }

    }
}
