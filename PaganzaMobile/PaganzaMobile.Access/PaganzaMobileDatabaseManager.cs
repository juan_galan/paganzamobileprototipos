﻿using System;
using PaganzaMobile.Commons.Database;
using SQLite;
using PaganzaMobile.Models;

namespace PaganzaMobile.Access
{
	public class PaganzaMobileDatabaseManager : DatabaseManager, IDisposable
	{
		public void Dispose ()
		{
			db.Dispose();
		}
			
		private const string DbName = "/data/data/com.PaganzaMobile/files/CanFindLocation/CanFindLocation.db";
		public SQLiteConnection db;

		public PaganzaMobileDatabaseManager()
		{
			db = new SQLiteConnection(DbName);
			db.CreateTable<Expense>();
			db.CreateTable<Service>();
			db.CreateTable<User>();
            db.CreateTable<Company>();
		}
	}
}

