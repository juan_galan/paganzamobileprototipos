﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace PaganzaMobile.Access
{
    public abstract class ApiAccess
    {
        public const string BaseUrL = "http://paganza-ort.cloudapp.net/DispMoviles/";

        public string Get(string uri, Dictionary<string,string> possibleParameters)
        {
            var request = new RestRequest(uri);
            request = AddParametersToRequest(request, possibleParameters);

            var response = MakeRequest(request, Method.GET);
            var result = this.ParseResponse(response.Content);

            return result;
        }

        public string Post(string uri, Dictionary<string, string> possibleParameters)
        {
            var request = new RestRequest(uri);
            request = AddParametersToRequest(request, possibleParameters);

            var response = MakeRequest(request, Method.POST);
            var result = this.ParseResponse(response.Content);

            return result;
        }

        public string Put(string uri, Dictionary<string, string> possibleParameters)
        {
            var request = new RestRequest(uri);
            request = AddParametersToRequest(request, possibleParameters);

            var response = MakeRequest(request, Method.PUT);
            var result = this.ParseResponse(response.Content);

            return result;
        }

        private IRestResponse MakeRequest(RestRequest request, Method method)
        {
            request.Method = method;
            request.RequestFormat = DataFormat.Json;
            var client = new RestClient(BaseUrL);
            return client.Execute(request);
        }

        private RestRequest AddParametersToRequest(RestRequest request, Dictionary<string,string> possibleParameters)
        {
            if(possibleParameters != null)
            {
                Dictionary<string,string> parameters = possibleParameters;
                foreach(string key in parameters.Keys)
                {
                    request.AddParameter(key, parameters[key]);
                }
            }
            return request;
        }

        private string ParseResponse(string rawResponse)
        {
            var result = rawResponse;
            result = result.Replace(@"\", "");
            result = result.Substring(1, result.Length - 2);

            return result;
        }
    }
}
