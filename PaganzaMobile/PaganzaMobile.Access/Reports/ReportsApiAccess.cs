﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using PaganzaMobile.Models;

namespace PaganzaMobile.Access.Reports
{
    public class ReportsApiAccess : ApiAccess
    {
        private const string OwnerIdParameter = "userId";
        private const string MonthParameter = "month";
        private const string YearParameter = "year";
        private const string MonthToParameter = "monthTo";
        private const string YearToParameter = "yearTo";
        private const string ServiceIdParameter = "serviceId";
        private const string CurrencyParameter = "currency";

        public MonthResponse GetMonthReport(int userId, int year, int month, int? yearTo, int? monthTo, int? serviceId, string currency)
        {
            MonthResponse report = new MonthResponse();
            try
            {
                Dictionary<string, string> parameters = this.GetQueryParameters(userId, year, month, yearTo, monthTo, serviceId, currency);
                string result = Get("ReportMonth", parameters);
                result = "{" + result + "}";
                dynamic expenseJson = JObject.Parse(result);
                

            }
            catch (Exception ex)
            {

            }

            return report;
        }

        private Dictionary<string, string> GetQueryParameters(int userId, int year, int month, int? yearTo, int? monthTo, int? serviceId, string currency)
        {
            var parameters = new Dictionary<string, string>();
            parameters[OwnerIdParameter] = userId.ToString();
            parameters[MonthParameter] = month.ToString();
            parameters[YearParameter] = year.ToString();
            parameters[CurrencyParameter] = currency.ToString();
            
            if(yearTo.HasValue)
            {
                parameters[YearToParameter] = yearTo.Value.ToString();
            }

            if (monthTo.HasValue)
            {
                parameters[MonthToParameter] = monthTo.Value.ToString();
            }

            if (serviceId.HasValue)
            {
                parameters[ServiceIdParameter] = serviceId.Value.ToString();
            }

            return parameters;

        }

    }
}
