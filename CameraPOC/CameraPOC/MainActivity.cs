﻿using System.IO;


namespace CameraPOC
{
	using System;
	using System.Collections.Generic;

	using Android.App;
	using Android.Content;
	using Android.Content.PM;
	using Android.Graphics;
	using Android.OS;
	using Android.Provider;
	using Android.Widget;
	using RestSharp;

	using Java.IO;

	using Environment = Android.OS.Environment;
	using Uri = Android.Net.Uri;

	public static class App{
		public static File _file;
		public static File _dir;    
		public static File _croppedImage;
		public static Bitmap bitmap;
	}

	[Activity (Label = "CameraPOC", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		private ImageView _imageView;
		private string filename;
		private TextView message;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.Main);

			if (IsThereAnAppToTakePictures())
			{
				CreateDirectoryForPictures();
				message = FindViewById<TextView> (Resource.Id.textView1);
				Button button = FindViewById<Button>(Resource.Id.myButton);
				Button button2 = FindViewById<Button>(Resource.Id.button1);

				_imageView = FindViewById<ImageView>(Resource.Id.imageView1);
				if (App.bitmap != null) {
					_imageView.SetImageBitmap (App.bitmap);
					App.bitmap = null;
				}
				button.Click += TakeAPicture;
				button2.Click += Recognize;
			}

		}

		private bool IsThereAnAppToTakePictures()
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);
			IList<ResolveInfo> availableActivities = PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
			return availableActivities != null && availableActivities.Count > 0;
		}

		private void CreateDirectoryForPictures()
		{
			App._dir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), "CameraAppDemo");
			if (!App._dir.Exists())
			{
				App._dir.Mkdirs();
			}
		}

		private void Recognize(object sender, EventArgs eventArgs)
		{
			var client = new RestClient("http://api.ocrapiservice.com/1.0/rest");
			var request = new RestRequest("ocr");
			request.AddParameter ("apikey", "rXPu23g3h3");
			request.AddParameter ("language", "es");
			request.Method = Method.POST;
			request.RequestFormat = DataFormat.Json;
			request.AddFile ("image", App._croppedImage.CanonicalPath);
			var response = client.Execute(request);
			message.Text = response.Content;
		}

		private void TakeAPicture(object sender, EventArgs eventArgs)
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);
			filename = String.Format("myPhoto_{0}.jpg", Guid.NewGuid ());
			App._file = new File(App._dir, filename);
			intent.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(App._file));

			StartActivityForResult(intent, 0);
		}

		private void performCrop(Uri picUri){
			Intent cropIntent = new Intent("com.android.camera.action.CROP");
			//indicate image type and Uri
			cropIntent.SetDataAndType(picUri, "image/*");
			//set crop properties
			cropIntent.PutExtra("crop", "true");
			//indicate output X and Y
			cropIntent.PutExtra("outputX", 256);
			cropIntent.PutExtra("outputY", 256);
			//retrieve data on return
			cropIntent.PutExtra("return-data", true);
			//start the activity - we handle returning in onActivityResult
			StartActivityForResult(cropIntent, 2);
		}

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			if (requestCode == 0) {
				
			
				// make it available in the gallery
				Intent mediaScanIntent = new Intent (Intent.ActionMediaScannerScanFile);
				Uri contentUri = Uri.FromFile (App._file);
				mediaScanIntent.SetData (contentUri);
				SendBroadcast (mediaScanIntent);



				performCrop (contentUri);
			}
			if (requestCode == 2) {
				Bundle extras = data.Extras;
				Bitmap image = (Bitmap)extras.GetParcelable ("data");
				// display in ImageView. We will resize the bitmap to fit the display
				// Loading the full sized image will consume to much memory 
				// and cause the application to crash.
				_imageView.SetImageBitmap(image);
				string filenamecrop = String.Format("myPhoto_{0}.jpg", Guid.NewGuid ());
				App._croppedImage = new File (App._dir, filenamecrop);
				using (var os = new FileStream(App._croppedImage.AbsolutePath, FileMode.CreateNew))
				{
					image.Compress(Bitmap.CompressFormat.Jpeg, 95, os);
				}
			}
		}
	}
}


